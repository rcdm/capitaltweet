# -*- encoding: utf-8 -*-
require File.expand_path('../lib/omniauth-oauth2-buffer/version', __FILE__)

Gem::Specification.new do |gem|
  gem.add_dependency 'omniauth', '~> 1.0'
  gem.add_dependency 'oauth2', '~> 0.6.0'

  gem.authors       = ["Richard Carey"]
  gem.email         = ["rc@rcdmcg.com"]
  gem.description   = %q{A Bufferapp.com OAuth2 strategy for OmniAuth.}
  gem.summary       = %q{A Bufferapp.com OAuth2 strategy for OmniAuth.}
  gem.homepage      = ""

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "omniauth-oauth2-buffer"
  gem.require_paths = ["lib"]
  gem.version       = OmniAuth::Oauth2buffer::VERSION
end
