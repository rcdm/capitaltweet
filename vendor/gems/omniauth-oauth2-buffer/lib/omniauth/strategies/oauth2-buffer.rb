require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Oauth2buffer < OmniAuth::Strategies::OAuth2
      option :name, "buffer"

      option :client_options, {
        :site => 'https://api.bufferapp.com',
        :authorize_url => 'https://bufferapp.com/oauth2/authorize',
        :token_url => 'https://api.bufferapp.com/1/oauth2/token.json'
      }
      
      option :access_token_options, {
        :header_format => 'OAuth %s',
        :param_name => 'access_token'
      }
      
      uid { access_token.params['user_id'] }

      info do
        {
        }
      end

      credentials do
        {
         }
      end

      def build_access_token
        super.tap do |token|
          token.options.merge!(access_token_options)
        end
      end

      def access_token_options
        options.access_token_options.inject({}) { |h,(k,v)| h[k.to_sym] = v; h }
      end
      
      def authorize_params
        super.tap do |params|
          params.merge!(:display => request.params['display']) if request.params['display']
        end
      end

      def raw_info
        access_token.options[:mode] = :query
        @raw_info ||= access_token.post('/1/profiles.json').parsed
      end
    end
  end
end

OmniAuth.config.add_camelization 'buffer', 'Buffer'