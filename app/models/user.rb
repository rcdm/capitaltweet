class User < ActiveRecord::Base
  attr_accessible :email, :name, :provider, :uid

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth['provider']
      user.uid = auth['uid']
      if auth['info']
         user.name = auth['info']['name'] || ""
         user.email = auth['info']['email'] || ""
         user.token = auth['credentials']['token'] || ""
         if auth['credentials']['refresh_token']
         user.secret = auth['credentials']['refresh_token'] || ""
         else
         user.secret = auth['credentials']['token'] || ""
         end
      end
    end
  end

end
