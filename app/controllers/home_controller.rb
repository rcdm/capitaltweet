class HomeController < ApplicationController

  def index
    @users = User.all
    @auth = request.env["omniauth.auth"]
    @user = current_user
  end
  
  def panel
     @user = current_user
     
     if (user_signed_in? )
        client = OAuth2::Client.new(
            "15212955717.apps.googleusercontent.com", "KYISer2Xb_q04lj5zvuAS6bR",
            :site => "https://accounts.google.com",
            :token_url => "/o/oauth2/token",
            :authorize_url => "/o/oauth2/auth")
        access_token = OAuth2::AccessToken.from_hash(client,
            {:refresh_token => current_user.secret})
        access_token = access_token.refresh!
        session = GoogleDrive.login_with_oauth(access_token)

        # First worksheet of
        # https://docs.google.com/spreadsheet/ccc?key=pz7XtlQC-PYx-jrVMJErTcg
        @ws = session.spreadsheet_by_key("0ArJbOFyntyO0dDVxOC1Ka0YyWHlRcGtSalBTUXNKb3c").worksheets[0]

         end
     
  end
  
  def about
  end
  
end
