class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :correct_user?

  def index
    @users = User.paginate(:page => params[:page])
  end

    def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to @user, :notice => 'Profile updated!'
    else
      render :edit
    end
  end


def show
    @user = User.find(params[:id])
    redirect_to edit_user_path
  end

end
